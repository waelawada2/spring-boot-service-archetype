#set ($hash = '#')
$hash ${serviceName.toUpperCase()}

$hash$hash Overview

// TODO: Fill in details here.

$hash$hash How to Build

Build the project with the embedded [Maven Wrapper](https://github.com/takari/maven-wrapper):

```
$ ./mvnw clean package
```

$hash$hash How to Test

Run unit tests with Maven:

```
$ ./mvnw clean test
```

$hash$hash How to Run

Run the application using the [spring-boot-maven-plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html):

```
$ ./mvnw spring-boot:run
```

This will launch the application and make it available on the configured port (property `server.port` in `application.yml`, 8081 by default):

```
$ curl http://localhost:8081/${serviceName.toLowerCase()}/v1/greeting
``` 

$hash$hash Swagger

Once the application is running, the Swagger file can be retrieved at [/${serviceName.toLowerCase()}/v1/docs/api](http://localhost:8081/${serviceName.toLowerCase()}/v1/docs/api):

```
$ curl http://localhost:8081/${serviceName.toLowerCase()}/v1/docs/api
```

Swagger UI is available at [/${serviceName.toLowerCase()}/v1/swagger-ui.html](http://localhost:8081/${serviceName.toLowerCase()}/v1/swagger-ui.html).

$hash$hash Actuator Endpoints

The actuator endpoints are mapped to the `/management` path.

For example, the Actuator health endpoint can be accessed as follows:

```
$ curl http://localhost:8081/${serviceName.toLowerCase()}/v1/management/health
```

$hash$hash Additional Tasks

- The generade code contains a dummy password to securty the actuator endpoints. Change the password with property `security.user.password` in the `application.yml` file. 
- Change the value of property `spring.cloud.config.failFast` to `true` as soon as a corresponding configuration repository has been set up and configured in the Config Server instances in ECS.

$hash$hash Additional Documentations

* [The Spring Boot Reference Guide](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/). The official Spring Boot reference documentation.
* [The Spring Cloud Reference Guide](http://cloud.spring.io/spring-cloud-static/Dalston.SR4/single/spring-cloud.html). The official Spring Cloud reference documentation for the Dalson release train.
