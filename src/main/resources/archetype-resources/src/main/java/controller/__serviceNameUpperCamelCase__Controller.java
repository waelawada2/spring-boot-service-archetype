package ${package}.controller;

import ${package}.service.${serviceNameUpperCamelCase}BusinessService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.Collections;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Sample controller.
 */
@RestController
@Api(produces = "application/json", value = "The service's main controller.")
@RequestMapping(value = "/greeting")
public class ${serviceNameUpperCamelCase}Controller {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(${serviceNameUpperCamelCase}Controller.class);

  private final ${serviceNameUpperCamelCase}BusinessService ${serviceNameLowerCamelCase}BusinessService;

  @Autowired
  public ${serviceNameUpperCamelCase}Controller(${serviceNameUpperCamelCase}BusinessService ${serviceNameLowerCamelCase}BusinessService) {
    this.${serviceNameLowerCamelCase}BusinessService = ${serviceNameLowerCamelCase}BusinessService;
  }

  @ApiResponses(value = {
    @ApiResponse(code = 200, message = "The request was processed successfull"),
    @ApiResponse(code = 500, message = "Server error occured")
  })
  @GetMapping("/{name}")
  @ResponseStatus(value = HttpStatus.OK)
  public Map<String, String> sayHello(@ApiParam(value = "The name to include in the greeting", required = true) @PathVariable String name) {
    return Collections.singletonMap("greeting", this.${serviceNameLowerCamelCase}BusinessService.getGreeting(name));
  }

}
