package ${package};

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Main entry point to the application.
 */
@SpringBootApplication
@EnableSwagger2
public class ${serviceNameUpperCamelCase}Application {

    /**
     * Main method for starting the application.
     */
    public static void main(String[] args) {
        SpringApplication.run(${serviceNameUpperCamelCase}Application.class, args);
    }

}
