#set($dollar = '$')
package ${package}.service.impl;

import ${package}.service.${serviceNameUpperCamelCase}BusinessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * The ${serviceName.toUpperCase()} service implementation.
 */
@Service
public class ${serviceNameUpperCamelCase}BusinessServiceImpl implements ${serviceNameUpperCamelCase}BusinessService {

    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(${serviceNameUpperCamelCase}BusinessServiceImpl.class);

    private final String greetingTemplate;

    public ${serviceNameUpperCamelCase}BusinessServiceImpl(@Value("${dollar}{greeting.template:Hello %s.}") String greetingTemplate) {
        this.greetingTemplate = greetingTemplate;
    }

    @Override
    public String getGreeting(String name) {
        return String.format(greetingTemplate, name);
    }

}
