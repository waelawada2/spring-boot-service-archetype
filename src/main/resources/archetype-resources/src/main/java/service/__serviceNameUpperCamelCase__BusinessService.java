package ${package}.service;

/**
 * The ${serviceName.toUpperCase()} business service interface.
 */
public interface ${serviceNameUpperCamelCase}BusinessService {

    /**
     * Get a greeting message.
     *
     * @return A greeting message with the given {@code name}.
     */
    String getGreeting(String name);

}
