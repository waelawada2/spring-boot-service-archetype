package ${package}.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import ${package}.service.${serviceNameUpperCamelCase}BusinessService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Tests the {@link ${serviceNameUpperCamelCase}Controller} using {@link MockMvc}.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ${serviceNameUpperCamelCase}Controller.class)
public class ${serviceNameUpperCamelCase}ControllerMvcTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private ${serviceNameUpperCamelCase}BusinessService ${serviceNameLowerCamelCase}BusinessService;

  @Before
  public void setUp() {
    when(${serviceNameLowerCamelCase}BusinessService.getGreeting("Sotheby's")).thenReturn("Hi Sotheby's");
  }

  /**
   * Tests the {@link ${serviceNameUpperCamelCase}Controller}'s response using a mocked {@link
   * ${serviceNameUpperCamelCase}BusinessService}.
   */
  @Test
  public void testGreeting() throws Exception {
    // @formatter:off
    mockMvc.perform(get("/greeting/{name}", "Sotheby's"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.greeting").value("Hi Sotheby's"));
    // @formatter:on
  }

}
