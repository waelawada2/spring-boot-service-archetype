package ${package};

import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Web integration tests using the {@link TestRestTemplate}.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ${serviceNameUpperCamelCase}WebIntegrationTest {

  @Autowired
  private TestRestTemplate restTemplate;

  /**
   * Tests the application's greeting endpoint.
   */
  @Test
  public void testSayHello() throws Exception {
    final ParameterizedTypeReference<Map<String, String>> responseType = new ParameterizedTypeReference<Map<String, String>>() {};
    ResponseEntity<Map<String, String>> responseEntity = this.restTemplate
        .exchange("/greeting/{name}", HttpMethod.GET, null, responseType, "Sotheby's");
    assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
    assertThat(responseEntity.getBody().size(), is(1));
    assertThat(responseEntity.getBody().get("greeting"), is("Hi Sotheby's!"));
    }

}
