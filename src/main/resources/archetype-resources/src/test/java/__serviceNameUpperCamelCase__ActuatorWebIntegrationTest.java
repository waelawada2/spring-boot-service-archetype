#set($dollar = '$')
package ${package};

import com.jayway.jsonpath.JsonPath;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Status;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Web integration tests for the application's actuator endpoints.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ${serviceNameUpperCamelCase}ActuatorWebIntegrationTest {

  @Autowired
  private TestRestTemplate restTemplate;

  @Value("${security.user.name}")
  private String username;

  @Value("${security.user.password}")
  private String password;

  @Value("${dollar}{management.contextPath:/management}")
  private String managementContextPath;

  /**
   * Tests the actuator health endpoint without details.
   */
  @Test
  public void testHealthEndpoint() throws Exception {
    // @formatter:off
    ResponseEntity<String> responseEntity = this.restTemplate
        .getForEntity(managementContextPath + "/health", String.class);
    // @formatter:on
    assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
    String body = responseEntity.getBody();
    assertThat(JsonPath.parse(body).read("$.status"), is(Status.UP.toString()));
  }

  /**
   * Tests the actuator health endpoint with details (authorization)
   */
  @Test
  public void testHealthEndpointWithDetails() throws Exception {
    // @formatter:off
    ResponseEntity<String> responseEntity = this.restTemplate
        .withBasicAuth(username, password)
        .getForEntity(managementContextPath + "/health", String.class);
    // @formatter:on
    assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
    String body = responseEntity.getBody();
    assertThat(JsonPath.parse(body).read("$.status"), is(Status.UP.toString()));
    assertThat(JsonPath.parse(body).read("$.diskSpace.status"), is(Status.UP.toString()));
  }

  /**
   * Tests the actuator info endpoint
   */
  @Test
  public void testInfoEndpoint() throws Exception {
    // @formatter:off
    ResponseEntity<String> responseEntity = this.restTemplate
        .getForEntity(managementContextPath + "/info", String.class);
    // @formatter:on
    assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
    String body = responseEntity.getBody();
    assertThat(JsonPath.parse(body).read("$.build.artifact"), is("${artifactId}"));
  }

}
