# Spring Boot Service Archetype ChangeLog

## 1.1.0

* Disable Spring Cloud Config client health indicator: Application properties are only retrieved from config server at startup. Therefore the config server health status is irrelevant during runtime and must not impact the health status of the application.
* Update Maven to version 3.5.3.
* Update Maven Wrapper to version 0.4.0.
* Increase memory for ECS deployments to STG and PRD due to higher memory requirements by OverOps agent.

## 1.0.1

* Fix placeholder filtering issues (in ECS task definition files and Dockerfile)

## 1.0.0

* Initial version
